package com.withdraw_cash;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner scanner = new Scanner(System.in);
        int count;
        int mod;
        int money = 0;
        System.out.println("Input");
        try{
            money = scanner.nextInt();
        }catch (InputMismatchException e){
            System.out.println("Exception please enter number 0 - 9. OR Enter numbers less than 2147483647.");
//            e.printStackTrace();
        }

        //-------- 100 -------------
        count = money / 100;
        //--------- 20 -------------
        mod = money % 100;
        count += mod / 20;
        //--------- 10 -------------
        mod = money % 20;
        count += mod / 10;
        //--------- 5 -------------
        mod = money % 10;
        count += mod / 5;
        //--------- 1 -------------
        mod = money % 5;
        count += mod;
        System.out.println("Output");
        System.out.println(count);
    }
}
